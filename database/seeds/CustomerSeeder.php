<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{ 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        foreach (range(1,6) as $index) {
        DB::table('customers')->insert([
            'last_name' => Str::random(10),
            'first_name' => Str::random(10),
            'phone' => rand(256700000000, 256799999999),
            'email' => Str::random(10).'@yahoo.com',
        ]);
        }
    }
}
