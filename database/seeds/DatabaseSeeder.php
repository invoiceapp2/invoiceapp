<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //function to seed Customers table
        
    $this->call([
        CustomerSeeder::class,
        ProductSeeder::class,
        InvoiceSeeder::class,
        InvoiceItemSeeder::class,
    ]);
    }
}
