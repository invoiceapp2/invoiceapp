<?php

use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
    $faker = \Faker\Factory::create();
    $customerIDs = DB::table('customers')->pluck('id');  
    foreach (range(1,4) as $index) { 
        foreach (range(1,3) as $index) {
            DB::table('invoices')->insert([
                'customer_id' => $faker->randomElement($customerIDs),
                'date' => date('Y-m-d', strtotime( '+'.mt_rand(0,30).' days'))
            ]);
          }
        }
    }
}
