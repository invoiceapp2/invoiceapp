<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
    foreach (range(1,20) as $index) {
        DB::table('products')->insert([
            'name' => Str::random(10),
            'price' => rand(1, 9999)*100,
            'description' => Str::random(),
            'service' => rand(0, 1)
        ]);
        }
    }
}
