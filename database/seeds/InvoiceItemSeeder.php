<?php

use Illuminate\Database\Seeder;

class InvoiceItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $faker = \Faker\Factory::create();
    $invoiceIDs = DB::table('invoices')->pluck('id');
    $productIDs = DB::table('products')->pluck('id'); 
    foreach (range(1,4) as $index) {  
        foreach (range(1,4) as $index) { 
            foreach (range(1,3) as $index) {
                DB::table('invoice_items')->insert([
                    'invoice_id' => $faker->randomElement($invoiceIDs),
                    'product_id' => $faker->randomElement($productIDs)
                ]);
                        }
                    }
                }
            }
        }
