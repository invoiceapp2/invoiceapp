<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Customer;

Route::get('/test-my-api',function(){
    return ['message'=>'hello'];
});

Route::resource('products','ProductController');
Route::resource('customers','CustomerController');
Route::resource('invoices','InvoiceController');
Route::resource('invoiceitems','InvoiceItemController');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
