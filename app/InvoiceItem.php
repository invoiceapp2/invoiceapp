<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    //
    protected $fillable = [
        'invoice_id',
        'product_id',
        'email',
        'phone',
        'address'
    ];
}
