<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceItem;

class InvoiceItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
      
    public function index()
    {
        $invoiceitem = InvoiceItem::all();
        return response()->json(['status' => 'Success', 'data' => $invoiceitem]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $invoiceitem = new InvoiceItem();
        $invoiceitem->invoice_id = $request->invoice_id;
        $invoiceitem->product_id = $request->product_id;
        $invoiceitem->save();

        return response()->json(['status' => 'Success', 'message' => 'InvoiceItem Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoiceitem = InvoiceItem::find($id);
        return response()->json(['status' => 'Success', 'data' => $invoiceitem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = InvoiceItem::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->save();

        return response()->json(['status' => 'Success', 'message' => 'Product(s) Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = InvoiceItem::find($id);
        $product->delete();
        return response()->json(['status' => 'Success', 'Message' => 'Product Deleted']);
    }
}
