
  $(document).ready(function(){
		var trigger = $('body div section div div div div div form div button, div form div button, div aside ul ul li a, div aside ul li ul li a, div form div div button, div div div div div form div div button, div div div div form div div button, div div div form div button, div div div div div form div div a, div form div div a, div div div div table tr td a, div div div div table tr td a, div div div div div form div div div a'),
		container = $('#main-content');
		trigger.on('click', function(){
		
				var $this = $(this),
				target = $this.data('target');
				
				container.load(target);
				
				// stop normal link behaviour
				return false;
				});
				});
				
   $('#cancel').click(function(){
				$.fancybox.close();
				return false;
				});

   $('#logout').click(function(){
		var url = 'content.php';
		var datatoPost ='view=attempt_logout';

		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			success:function(data){
				alert(datatoPost+' url='+url),
				location.reload();
				//$('#main-content').load('index.php');
							}
						});
				});

   $('#login_button').click(function(){
		var url = 'content.php';
		var email = $('#email').val();
		var pass = $('#password').val();
		var datatoPost ='view=attempt_login&email='+email+'&pass='+pass;

		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),
				document.getElementById("login_form")[0].reset(),
				$('#main-content').load('index.php');
				//container.load('content.php?view=shopping_cart');
							}
						});
				});

				/*
   $('#date_range_filter').click(function(){
		var from_date = $('#from_date').val();
		var to_date = $('#to_date').val();
		var datatoPost ='view=shopping_cart&from_date='+from_date+'&to_date='+to_date;
		
		var url = 'content.php?';
				//$('#main-content').load('content.php?view=stock');
				//container.load(url+datatoPost);
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				$('#main-content').load('content.php?view=stock');
				//$('#main-content').load(url+datatoPost);
							}
						});
					});*/

   $('#search_filter').click(function(){
		//var table = $('#table').val();
		//var column = $('#column').val();
		var pid = $('#stock_item').val();
		var view = $('#view').val();
		var datatoPost ='view='+view+'&pid='+pid;//&table='+table+'&column='+column+'
		
		var url = 'content.php?';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url);	
				$('#main-content').load(url+datatoPost);
							}
						});
					});

   $('#date_range_filter').click(function(){
		var from_date = $('#from_date').val();
		var to_date = $('#to_date').val();
		//var pid = $('#stock_item').val();
		var view = $('#view').val();
		var datatoPost ='view='+view+'&from_date='+from_date+'&to_date='+to_date;
		
		var url = 'content.php?';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url);	
				$('#main-content').load(url+datatoPost);
							}
						});
					});

   $('#date_filter').click(function(){
		var from_date = $('#single_date').val();
		var to_date = $('#single_date').val();
		var view = $('#view').val();
		var datatoPost ='view='+view+'&from_date='+from_date+'&to_date='+to_date;
		
		var url = 'content.php?';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				$.fancybox.close(),
				alert(datatoPost+' url='+url);	
				$('#main-content').load(url+datatoPost);
							}
						});
					});

   $('#clear_cart_button').click(function(){
		var url = 'content.php';
		var quantity_amount= $('#quantity_amount').val();
		var datatoPost ='view=clear_cart';
		
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				//document.getElementById("shopping_cart_form")[0].reset(),
				$('#main-content').load('content.php?view=shopping_cart');
				//container.load('content.php?view=shopping_cart');
							}
						});
				});

   $('#delete_cart_item').click(function(){
		var url = 'content.php';
		var $this = $(this);
		var item_id = $this.data('item');
		var datatoPost ='view=clear_cart&item_id='+item_id;

		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				//container.load('content.php?view=shopping_cart'),
				alert(datatoPost+' url='+url),
				//document.getElementById("shopping_cart_form")[0].reset(),
				$('#main-content').load('content.php?view=shopping_cart');
				//container.load('content.php?view=shopping_cart');
							}
						});
				});

   $('#add_sale_button').click(function(){
		//document.getElementById("add_sale_form").submit();
		var select_client= $('#selected_client').val();
		var sale_date= $('#sale_date').val();
		var sale_total= $('#sale_total').val();
		var reference_number= $('#reference_number').val();
		var payment_mode= $('#payment_mode').val();
		var datatoPost ='view=batch_sale_added&payment_mode='+payment_mode+'&reference_number='+reference_number+'&select_client='+select_client+'&sale_date='+sale_date+'&sale_total='+sale_total;

		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),
				$.fancybox.close(),
				document.getElementById("shopping_cart_form").reset(),
				document.getElementById("add_sale_form").reset(),
				$('#main-content').load('content.php?view=shopping_cart');
							}
						});
					});

				
   $('#make_sale_button').click(function(){
		var select_product= $('#select_product').val();
		var transaction_date= $('#transaction_date').val();
		var quantity_map_id= $('#quantity_map_id').val();
		var quantity_amount= $('#quantity_amount').val();
		var select_client= $('#select_client').val();
		var payment_mode= $('#payment_mode').val();
		//if($('#credit').val()==''){var payment_mode= 'cash';
		//}else{var payment_mode= 'credit';}
		var datatoPost ='view=item_added&select_product='+select_product+'&transaction_date='+transaction_date+'&quantity_map_id='+quantity_map_id+'&quantity_amount='+quantity_amount+'&select_client='+select_client;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),
				document.getElementById("shopping_cart_form").reset(),
				document.getElementById("make_sale_form").reset(),
				$('#main-content').load('content.php?view=shopping_cart'),
				$.fancybox.close();
							}
						});
					});

   $('#stock_take_button').click(function(){
		var select_product= $('#select_product').val();
		var stock_take_date= $('#stock_take_date').val();
		var quantity_map_id= $('#quantity_map_id').val();
		var quantity_amount= $('#quantity_amount').val();
		var datatoPost ='view=stock_counted&select_product='+select_product+'&stock_take_date='+stock_take_date+'&quantity_map_id='+quantity_map_id+'&quantity_amount='+quantity_amount;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),
				document.getElementById("stock_take_form").reset(),
				$.fancybox.close();
							}
						});
					});
					

   /* $('#shopping_cart_button').click(function(){
		var select_product= $('#select_product').val();
		var transaction_date= $('#transaction_date').val();
		var quantity_map_id= $('#quantity_map_id').val();
		var quantity_amount= $('#quantity_amount').val();
		var select_client= $('#select_client').val();
		var cart_item= $('#cart_item').val();
		var datatoPost ='view=shopping_cart&select_product='+select_product+'&transaction_date='+transaction_date+'&quantity_map_id='+quantity_map_id+'&quantity_amount='+quantity_amount+'&select_client='+select_client+'&cart_item='+cart_item;
		
		//$.getJSON("content.php?view=product_detail&product="+select_product, function( data ){
			if (sessionStorage.clickcount && Number(sessionStorage.clickcount)>0) {
			sessionStorage.clickcount = Number(sessionStorage.clickcount)+1;
			cart = [sessionStorage.clickcount,data[0].ProductID,data[0].ProductName,data[0].Price,'<tr id="row'+data[0].ProductID+'"><td>'+data[0].ProductName+'</td><td>'+'</td><td>'+data[0].Price+'</td><td>'+ data[0].Price.toLocaleString('en-US', { style: 'currency', currency: 'UGX' }) +'</td><td><a class="btn btn-circle show-tooltip" title="Delete selected" href="#" onClick="clear_row('+data[0].ProductID+')"><i class="fa fa-trash-o"></i></a></td><td><input type="hidden" name="product'+data[0].ProductID+'" id="product'+data[0].ProductID+'" value="'+data[0].ProductID+'"></td><td>'+sessionStorage.clickcount+'</td></tr>'];
			sessionStorage.tabledata = sessionStorage.tabledata+cart[5];
			} else {
			sessionStorage.clickcount = 1;
		var cart = [sessionStorage.clickcount,data[0].ProductID,data[0].ProductName,data[0].Price,'<tr id="row'+data[0].ProductID+'"><td>'+data[0].ProductName+'</td><td>'+'</td><td>'+data[0].Price+'</td><td>'+ data[0].Price.toLocaleString('en-US', { style: 'currency', currency: 'UGX' }) +'</td><td><a class="btn btn-circle show-tooltip" title="Delete selected" href="#" onClick="clear_row('+data[0].ProductID+')"><i class="fa fa-trash-o"></i></a></td><td><input type="hidden" name="product'+data[0].ProductID+'" id="product'+data[0].ProductID+'" value="'+data[0].ProductID+'"></td><td>'+sessionStorage.clickcount+'</td></tr>'];
			sessionStorage.tabledata = cart[4];
			//sessionStorage.totalcost = ($("#Quantity").val()*data[0].Cost);
			}
			document.getElementById("list_body").innerHTML = '<tr id="row'+data[0].ProductID+'"><td>'+data[0].ProductName+'</td><td>'+'</td><td>'+data[0].Price+'</td><td>'+ data[0].Price.toLocaleString('en-US', { style: 'currency', currency: 'UGX' }) +'</td><td><a class="btn btn-circle show-tooltip" title="Delete selected" href="#" onClick="clear_row('+data[0].ProductID+')"><i class="fa fa-trash-o"></i></a></td><td><input type="hidden" name="product'+data[0].ProductID+'" id="product'+data[0].ProductID+'" value="'+data[0].ProductID+'"></td><td>'+sessionStorage.clickcount+'</td></tr>';
		//}
  		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				//alert(datatoPost+' url='+url);	
				document.getElementById("shopping_cart_form").reset();
							}
						});
					}); */
					

   $('#shopping_cart_button').click(function(){
		var select_product= $('#select_product').val();
		var transaction_date= $('#transaction_date').val();
		var quantity_map_id= $('#quantity_map_id').val();
		var quantity_amount= $('#quantity_amount').val();
		var select_client= $('#select_client').val();
		var cart_item= $('#cart_item').val();
		var datatoPost ='view=store_cart&select_product='+select_product+'&transaction_date='+transaction_date+'&quantity_map_id='+quantity_map_id+'&quantity_amount='+quantity_amount+'&select_client='+select_client+'&cart_item='+cart_item;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				//alert(datatoPost+' url='+url);	
				//document.getElementById("shopping_cart_form").reset(),
				$('#main-content').load('content.php?view=shopping_cart');
							}
						});
					});


   $('#add_expense_button').click(function(){
		var customer_id= 1;
		var fund_source= 'expense';
		var expense_description= $('#expense_description').val();
		var amount= $('#amount').val();
		var receipt_number= 'EXP';
		var datatoPost ='view=account_credited&fund_source='+fund_source+'&customer_id='+customer_id+'&receipt_number='+receipt_number+'&amount='+amount+'&expense_description='+expense_description;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_expense_form").reset(),
				$('#main-content').load('content.php?view=expenses'),
				$.fancybox.close();
							}
						});
					});
					
   $('#date_closure_button').click(function(){
		var close_date= $('#close_date').val();
		var closure_notes= $('#closure_notes').val();
		var datatoPost ='view=date_closure_added&close_date='+close_date+'&closure_notes='+closure_notes;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("date_closure_form").reset(),
				$('#main-content').load('content.php?view=date_closure'),
				$.fancybox.close();
							}
						});
					});
					
   $('#add_price_button').click(function(){
		var price= $('#price').val();
		var quantity_map_id= $('#quantityMap_id').val();
		var product_id= $('#product_id').val();
		var datatoPost ='view=price_added&product_id='+product_id+'&quantity_map_id='+quantity_map_id+'&price='+price;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_price_form").reset(),
				$('#main-content').load('content.php?view=shopping_cart'),
				$.fancybox.close();
							}
						});
					});
				
				
   $('#add_customer_button').click(function(){
		var customer_name= $('#customer_name').val();
		var customer_email= $('#customer_email').val();
		var customer_phone= $('#customer_phone').val();
		var customer_profile= $('#customer_profile').val();
		var datatoPost ='view=customer_added&customer_name='+customer_name+'&customer_email='+customer_email+'&customer_phone='+customer_phone+'&customer_profile='+customer_profile;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_expense_form").reset(),
				//$('#main-content').load('content.php?view=customer'),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=customer');
							}
						});
					});
				
				
   $('#add_user_button').click(function(){
		var first_name= $('#first_name').val();
		var last_name= $('#last_name').val();
		var user_email= $('#user_email').val();
		var user_phone= $('#user_phone').val();
		var user_level= $('#user_level').val();
		var password= $('#password').val();
		var datatoPost ='view=user_added&first_name='+first_name+'&last_name='+last_name+'&user_email='+user_email+'&user_phone='+user_phone+'&user_level='+user_level+'&password='+password;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_expense_form").reset(),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=employees');
							}
						});
					});
				
				
   $('#add_shop_button').click(function(){
		var shop_name= $('#shop_name').val();
		var shop_location= $('#shop_location').val();
		var shop_profile= $('#shop_profile').val();
		var shop_admin= $('#shop_admin').val();
		var active= $('#active').val();
		var datatoPost ='view=shop_added&shop_name='+shop_name+'&shop_location='+shop_location+'&shop_profile='+shop_profile+'&shop_admin='+shop_admin+'&active='+active;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_shop_form").reset(),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=shop');
							}
						});
					});
				
				
   $('#add_branch_button').click(function(){
		var branch_name= $('#branch_name').val();
		var branch_location= $('#branch_location').val();
		var branch_profile= $('#branch_profile').val();
		var branch_admin= $('#branch_admin').val();
		var active= $('#active').val();
		var datatoPost ='view=branch_added&branch_name='+branch_name+'&branch_location='+branch_location+'&branch_profile='+branch_profile+'&branch_admin='+branch_admin+'&active='+active;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_branch_form").reset(),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=branch');
							}
						});
					});
					
				
   $('#credit_client_account').click(function(){
		var customer_id= $('#customer_id').val();
		var fund_source= $('#fund_source').val();		
		var amount= $('#amount').val();
		var receipt_number= $('#receipt_number').val();
		var datatoPost ='view=account_credited&fund_source='+fund_source+'&customer_id='+customer_id+'&receipt_number='+receipt_number+'&amount='+amount;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("credit_client_form").reset(),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=customer');
							}
						});
					});
					
   $('#add_quantity_button').click(function(){
		var quantifier_name= $('#quantifier_name').val();
		var number_of_items= $('#number_of_items').val();
		var quantifier_description= $('#quantifier_description').val();
		var datatoPost ='view=quantity_added&quantifier_name='+quantifier_name+'&number_of_items='+number_of_items+' &quantifier_description='+quantifier_description;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_quantity_form").reset(),
				$.fancybox.close(),
				$('#main-content').load('content.php?view=products');
							}
						});
					});
				
				
   $('#add_stock_button').click(function(){
		var product_id= $('#select_product').val();
		var transaction_date= $('#transaction_date').val();
		var quantity_map_id= $('#select_quantity_map').val();
		var quantity_amount= $('#quantity_amount').val();
		var datatoPost ='view=stock_added&product_id='+product_id+'&transaction_date='+transaction_date+' &quantity_map_id='+quantity_map_id+' &quantity_amount='+quantity_amount;
		
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_stock_form").reset(),
				$('#main-content').load('content.php?view=stock'),
				$.fancybox.close();
							}
						});
					});

   $('#add_product_button').click(function(){
		var product_name= $('#product_name').val();
		var product_code= $('#product_code').val();
		var product_description= $('#product_description').val();
		var select_category= $('#select_category').val();
		var enter_category= $('#enter_category').val();
		var datatoPost ='view=product_added&product_name='+product_name+'&product_code='+product_code+' &product_description='+product_description+' &select_category='+select_category+' &enter_category='+enter_category;
		
		//var url = 'content.php';
		var url = 'content.php';
		var $this = $(this);
		$.ajax({
			type:'POST',
			data: datatoPost,
			url: url,
			//dataType: "json",
			success:function(data){
				alert(datatoPost+' url='+url),	
				document.getElementById("add_product_form").reset(),
				$.fancybox.close();
				$('#main-content').load('content.php?view=products');
					}
					});
				});
				
				
	$('.fancybox').fancybox();
	$(".new_record").fancybox({
		type : 'iframe',
		padding : 5
	});