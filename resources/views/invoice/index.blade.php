<!DOCTYPE html>

<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
  <title>INVOICE APP</title>

  <link href="{{ URL::asset('css/modules/bootstrap/css/bootstrap.min ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/modules/ionicons/css/ionicons.min ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min ')}}" rel="stylesheet" type="text/css" />

  <link href="{{ URL::asset('css/modules/summernote/summernote-lite ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/modules/flag-icon-css/css/flag-icon.min ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/css/style ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/modules/ionicons/css/ionicons.min ')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('css/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min ')}}" rel="stylesheet" type="text/css" />

  <link href="{{ URL::asset('css/fancybox-2.1.7/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link href="{{ URL::asset('css/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
  <link href="{{ URL::asset('css/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
			
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="main-content" id="main-content">
		<section class="section">

    <h1 class="section-header">
				<div>Products </div>
				<div Style="float:right">
				<a class="fancybox fancybox.ajax" href="content.php?view=add_product">
				<i class="ion " title="Add new Product">Add Product</i></a></div>
			  </h1>
        
				<div class="row mt-5">
				  <div class="col-12">
					<div class="card">
					  <div class="card-header">
					   <h4>Product List</h4>
						<div class="float-left">
						 <form>
						  <div class="row">
						  <div class="form-group col-4">
						  <label for="from_date">From Date:</label>
						  <input id="from_date" type="date" class="form-control" name="from_date" required>
						  </div>
						  <div class="form-group col-4">
						  <label for="to_date">To Date:</label>
						  <input id="to_date" type="date" class="form-control" name="to_date" required>
						  </div>
						  <div class="form-group col-4" Style="float:right">
						  <label for="single_date">Single Date:</label>
						  <input id="single_date" type="date" class="form-control" name="single_date" required>
						  <input id="view" type="hidden" value="products">
						  </div>
						  </div>
						  <a href="#" class="btn btn-action btn-secondary" id="date_range_filter">Filter Date Range</a>
						  <a href="#" class="btn btn-action btn-secondary" id="date_filter" Style="margin-left:260px">Filter By Date</a>
						 </form>
					  </div>
					  <div class="float-right">
						<form>
						 <div class="form-group col-1"></br></br></div>
						<div class="input-group">
						<input type="text" class="form-control" id="search_table" placeholder="Search">
						<div class="input-group-btn">
						<button class="btn btn-secondary"><i class="ion ion-search"></i></button>
						</div>
						</div>
						</form>
					  </div>
					</div>
					<div class="card-body">
					
					<table class="table table-striped" id="product_table">
					<thead><tr>
					  <th class="text-center">
						<div class="custom-checkbox custom-control">
						  <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
						  <label for="checkbox-all" class="custom-control-label"></label>
						</div>
					  </th>
					  <th>#</th>
					  <th>Product Name</th>
					  <th>Category</th>
					  <th>Stocked</th>
					  <th>Sold</th>
					  <th>Available</th>
					  <th>Created By</th>
					  <th>Date Created</th>
					  <th>Description</th>
					</tr></thead>
					<tfoot Style="color:purple"><tr>
					  <th></th>
					  <th></th>
					  <th>Product Name</th>
					  <th>Category</th>
					  <th>Stocked</th>
					  <th>Sold</th>
					  <th>Available</th>
					  <th>Created By</th>
					  <th>Date Created</th>
					  <th></th>
					</tr></tfoot>
					<tbody>
					<?php
					$data_to_fetch = 'products1';
					
					if( (isset($_GET['from_date']) && $_GET['from_date']!='')&&(isset($_GET['to_date']) && $_GET['to_date']!='') ){$to_date=$_GET['to_date'];$from_date=$_GET['from_date'];
					}else if(isset($_GET['single_date']) && $_GET['single_date']!=''){$from_date=$to_date=$_GET['single_date'];
					}else{$from_date=$to_date=null;}
					//$api->json_table_data($data_to_fetch,$from_date,$to_date);?>
						   </tbody>
						  </table>
						 </div>
						</div>
					   </div>
					  </div>
					<script type="text/javascript" src="dist/fancybox-2.1.7/source/jquery.fancybox.pack.js?v=2.1.5"></script>
					<script src="dist/modules/toastr/build/toastr.min.js"></script>
					<script src="dist/js/beauty.js"></script>
					<script src="dist/js/jquery.3.2.1.min.js"></script>
					<script src="dist/js/datatables.min.js"></script>
					<script type="text/javascript">
					$('#product_table tfoot th').each(function() {

					  var title = $(this).text();
					  if(title !== ''){
					  $(this).append('<br/><input type="text" placeholder="Search ' + title + '" Style="width:100px"/>');
					  }
					});
					// DataTable
					var table = $('#product_table').DataTable({
					"iDisplayLength": 5,
					"dom":"tipr"
					});

					// Apply the search
					table.columns().every(function() {
					  var that = this;

					  $('input', this.footer()).on('keyup change', function() {
						if (that.search() !== this.value) {
						  that.search(this.value).draw();
						}
					  });
					});

					$('#search_table').on('keyup change', function(){
					  table.search($(this).val()).draw();
					});
					
					$('.fancybox').fancybox();
					$(".new_record").fancybox({
						type : 'iframe',
						padding : 5
					});
					</script>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://multinity.com/">Multinity</a>
        </div>
        <div class="footer-right"></div>
      </footer>
    </div>
  </div>
  
<div class="main-sidebar"> 
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.php">WELCOME</a>
          </div>
          <div class="sidebar-user">
          
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="active">
              <a href="index.php"><i class="ion ion-speedometer"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">DEFAULT</li>
            
            <ul class="sidebar-menu">
              <li><a href="#" data-target="content.php?view=shopping_cart">
                <i class="ion ion-clipboard"></i>Invoices</a>
            </li>
            </ul>
            
            <ul class="sidebar-menu">
              <li><a href="#" data-target="content.php?view=shopping_cart">
                <i class="ion ion-ios-albums-outline"></i>Products</a>
            </li>
            </ul>
            
            <ul class="sidebar-menu">
              <li><a href="#" data-target="content.php?view=shopping_cart">
                <i class="ion ion-ios-people"></i>Customers</a>
            </li>
            </ul>

          <div class="p-3 mt-4 mb-4">
            <a href="#" class="btn btn-danger btn-shadow btn-round has-icon has-icon-nofloat btn-block">
              <i class="ion ion-help-buoy"></i> <div>READY!</div>
            </a>
          </div>
        </aside>
      </div> 
  <script src="{{ URL::asset('css/modules/jquery.min.js"></script>
  <script src="{{ URL::asset('css/modules/popper.js"></script>
  <script src="{{ URL::asset('css/modules/tooltip.js"></script>
  <script src="{{ URL::asset('css/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="{{ URL::asset('css/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="{{ URL::asset('css/modules/scroll-up-bar//../../extras/scroll-up-bar.min.js"></script>
  <script src="{{ URL::asset('css/js/sa-functions.js"></script>
  <script src="{{ URL::asset('css/js/beauty.js"></script>
  <script type="text/javascript" src="{{ URL::asset('css/fancybox-2.1.7/source/jquery.fancybox.pack.js?v=2.1.5"></script>
  <script type="text/javascript" src="{{ URL::asset('css/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script type="text/javascript" src="{{ URL::asset('css/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
  <script type="text/javascript" src="{{ URL::asset('css/fancybox-2.1.7/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
  
  <script src="{{ URL::asset('css/modules/chart.min.js"></script>
  <script src="{{ URL::asset('css/modules/summernote/summernote-lite.js"></script>
  
  <script src="{{ URL::asset('css/js/scripts.js"></script>
  <script src="{{ URL::asset('css/js/custom.js"></script>
  
  <script type="text/javascript">
	$('.fancybox').fancybox();
	$(".new_record").fancybox({
		type : 'iframe',
		padding : 5
	});
	</script>
  <script>
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      datasets: [{
        label: 'Statistics',
        data: [460, 458, 330, 502, 430, 610, 488],
        borderWidth: 2,
        backgroundColor: 'rgb(87,75,144)',
        borderColor: 'rgb(87,75,144)',
        borderWidth: 2.5,
        pointBackgroundColor: '#ffffff',
        pointRadius: 4
      }]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            stepSize: 150
          }
        }],
        xAxes: [{
          gridLines: {
            display: false
          }
        }]
      },
    }
  });
  </script>
</body>
</html>